//
//  BarCodeScanner.h
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/12/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GraphicsLib.h"
#import <UIKit/UIKit.h>
#import "WebViewController.h"
#import "ViewController.h"
#import "TakePictureController.h"

@interface BarCodeScanner : UIViewController<AVCaptureMetadataOutputObjectsDelegate, UIWebViewDelegate, UISearchBarDelegate, UIScrollViewDelegate>{

}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection;

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
- (void)webViewDidStartLoad:(UIWebView *)webView;
- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;

+(void) setDefaultUrlLoad:(NSString *) append;
+(UIButton *) getExternalNotification;

+(void) setbarCodeScannerButtonStatusYES;
+(void) setbarCodeScannerButtonStatusNO;

@end
