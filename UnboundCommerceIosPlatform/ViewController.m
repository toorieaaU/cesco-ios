//
//  ViewController.m
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/10/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (){

}
@end

@implementation ViewController
UIButton *webViewButton;
UIButton *scannerButton;
UIButton *takePhotoButton;

- (void)viewDidAppear:(BOOL) animated{
	NSLog(@"view appeared for View Controller Main");
	[super viewDidAppear:YES];
}

- (void)viewDidLoad {

	{ //delete cookies on startup
		NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
		for (NSHTTPCookie *cookie in [storage cookies]) {
			if([[cookie name] isEqualToString:@"compareCescoNumbers"]){
				[storage deleteCookie:cookie];
			}
		}
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
	{//isolated scope
		CGRect screenRect = [[UIScreen mainScreen] bounds];
		CGFloat screenWidth = screenRect.size.width;
		CGFloat screenHeight = screenRect.size.height;

		[GraphicsLib setScreenWidth:screenWidth];
		[GraphicsLib setScreenHeight:screenHeight];

		NSLog(@"screenWidthCapture%d", [GraphicsLib getScreenWidth]);
		NSLog(@"screenHeightCapture%d", [GraphicsLib getScreenHeight]);
	}

	/* NSLog(@"%@",[Resource getButtonColorAsString]);
	 NSLog(@"%@",[Resource getButtonColorOverAsString]);
	 NSLog(@"%@",[GraphicsLib getColor:[Resource getButtonColorAsString]]);*/
	webViewButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[GraphicsLib setButtonStyleForBottomMostButton:webViewButton withText:@"WEB MODULE"];

	takePhotoButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[GraphicsLib setButtonStyleForBottomMostButton:takePhotoButton withText:@"TAKE PHOTO (TESTING)"];

	scannerButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	[GraphicsLib setButtonStyleForBottomMostButton:scannerButton withText:@"SCAN PRODUCT CODE"];

	[GraphicsLib setButtonAtYPosition:scannerButton withNewAtY:[GraphicsLib getScreenHeight] - [GraphicsLib getHeightFromPercentageAsPixels:22]];

	[GraphicsLib setButtonAtYPosition:takePhotoButton withNewAtY:[GraphicsLib getScreenHeight] - [GraphicsLib getHeightFromPercentageAsPixels:32]];



	UIView *myBackgroundImage = [GraphicsLib getImageAsBackground:@"cesco.png"];

	[self.view addSubview:myBackgroundImage];
	[self.view addSubview:webViewButton];
	[self.view addSubview:scannerButton];
	[self.view addSubview:takePhotoButton];

	[webViewButton addTarget:self action:@selector(onClickToWebView:) forControlEvents:UIControlEventTouchDown];

	[webViewButton addTarget:self action:@selector(onReleaseToWebView:) forControlEvents:UIControlEventTouchUpInside];

	[scannerButton addTarget:self action:@selector(onClickToScannerCode:) forControlEvents:UIControlEventTouchDown];

	[scannerButton addTarget:self action:@selector(onReleaseToScannerCode:) forControlEvents:UIControlEventTouchUpInside];

	[takePhotoButton addTarget:self action:@selector(onTakePhotoModule:) forControlEvents:UIControlEventTouchDown];

	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(void)onClickToWebView:(id)sender{
	UIButton *button = (UIButton *)sender;
	[GraphicsLib setButtonOpacityWithColor:button withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorOverAsString]]];
}

-(void)onReleaseToWebView:(id)sender{
	UIButton *button = (UIButton *)sender;
	[GraphicsLib setButtonOpacityWithColor:button withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorAsString]]];
	WebViewController * vc = [[WebViewController alloc] init];
	[self presentViewController:vc animated:YES completion:nil];
}

-(void)onClickToScannerCode:(id)sender{
	UIButton *button = (UIButton *)sender;
	[GraphicsLib setButtonOpacityWithColor:button withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorOverAsString]]];
}

-(void)onReleaseToScannerCode:(id)sender{
	UIButton *button = (UIButton *)sender;
	[GraphicsLib setButtonOpacityWithColor:button withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorAsString]]];
	BarCodeScanner * vc = [[BarCodeScanner alloc] init];
	[self presentViewController:vc animated:YES completion:nil];
}

-(void)onTakePhotoModule:(id)sender{
	UIButton *button = (UIButton *)sender;
	[GraphicsLib setButtonOpacityWithColor:button withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorAsString]]];
	TakePictureController * vc = [[TakePictureController alloc] init];
	[self presentViewController:vc animated: NO completion:nil];
}

-(NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}

@end
