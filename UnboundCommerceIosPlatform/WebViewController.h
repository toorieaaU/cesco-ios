//
//  WebViewController.h
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/11/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphicsLib.h"
#import "Resource.h"
#import "BarCodeScanner.h"


@interface WebViewController : UIViewController<UIWebViewDelegate, UISearchBarDelegate, UIScrollViewDelegate>{
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
- (void)webViewDidStartLoad:(UIWebView *)webView;
- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (NSString*) urlRequestToString:(NSURLRequest*)urlRequest;
-(void) setBaseUrl:(NSString * )myNewBase;
+(BOOL) cookieValueIsNull ;
+(dispatch_semaphore_t) getPictureLock ;

@end
