//
//  TakePictureController.m
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 7/2/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TakePictureController.h"


@interface TakePictureController (){

}
@end

UIImagePickerController *imagePickerController;
UIImage *thisImage;
BOOL isImagePickerOn;


@implementation TakePictureController

+(UIImage * ) getImage {
	return thisImage;
}

+(void) initialize{
	imagePickerController = [[UIImagePickerController alloc] init];
	isImagePickerOn = YES;
}

- (void)viewDidLoad {
	[self.view setBackgroundColor:[GraphicsLib colorWithHexString:@"#FFF"]];
	[super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated{
	if(isImagePickerOn){
		imagePickerController = [[UIImagePickerController alloc] init];
		imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
		[imagePickerController setDelegate:self];
		isImagePickerOn = NO;
		[self takePhoto:self];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	
	
	[self dismissViewControllerAnimated:YES completion:^{
		UIViewController *presentingVC = [self presentingViewController];
		[self dismissViewControllerAnimated:NO completion:
		 ^{
			 isImagePickerOn = YES;
		 }];
	}];
}


- (void)takePhoto:(id)sender
{
	// Lazily allocate image picker controller
	if (!imagePickerController) {


		// If our device has a camera, we want to take a picture, otherwise, we just pick from
		// photo library
		if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
		{
			[imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
		}else
		{
			[imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
		}

		// image picker needs a delegate so we can respond to its messages

	}
	// Place image picker on the screen
	[self presentViewController:imagePickerController animated:NO completion:nil];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	[self dismissViewControllerAnimated:NO completion:nil]; //Do this first!!

	UIImageView *imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [GraphicsLib getScreenWidth], [GraphicsLib getScreenHeight])];

	UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
	thisImage = image;

	

	imageHolder.image = thisImage;

	NSLog(@"Debug description of image%@", [image debugDescription]);

	MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
	mail.mailComposeDelegate = self;
	[mail setSubject:@"Submit Photo to Support Team (CESCO)"];
	[mail setMessageBody:@"Hello, This is ${customer.name} <br/> I am having a problem ... Here I have attached a photo for you to review." isHTML:YES];

	[mail addAttachmentData:[NSData dataWithData:UIImageJPEGRepresentation(image, .75f)] mimeType:@"image/jpeg" fileName:@"image.jpg"];
	mail.mailComposeDelegate = self;

	NSArray *usersTo = [NSArray arrayWithObjects: @"toorieaa@unboundcommerce.com", @"ivor@unboundcommerce.com", nil];

	[mail setToRecipients:usersTo];

	dispatch_semaphore_signal([WebViewController getPictureLock]);


	[self presentViewController:mail animated:YES completion:NULL];

	//[self.view addSubview:imageHolder];
}

-(void)mailComposeController:(MFMailComposeViewController *)mailer didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
	

	NSLog(@"Mailing error is: %@", [error description]);

	[self dismissViewControllerAnimated:YES completion:^{
		UIViewController *presentingVC = [self presentingViewController];
		[self dismissViewControllerAnimated:NO completion:
		 ^{
			 isImagePickerOn = YES;
		 }];
	}];
}




@end








