//
//  WebViewController.m
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/11/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController (){
	NSString * lastUrl;
	NSString *updateValueUrl;

	UIWebView *mainWebViewMobileSite;
	UIWebView *webViewNavBar;

	UIView * mySepBarImage;
	UIView* loadingView;
	UIView* barCodeIcon;
	UIView* notificationBoxImage;
	UIView* backgroundForSearchState;
	UIView * myBackgroundImage;
	UIView * mySearchToggleIcon;

	UILabel *menuLabel;
	UILabel *textForNotificationBox;

	UIButton * myButtonForMenu;
	UIButton * minimizeButtonState;
	UIButton * disableBlurButton;
	UIButton * myButtonForClickEventSearch;
	UIButton * rotateScreenToDisplayEnlargedViewButtonOnClickNothing;

	UIButton *titleAccount;
	UIButton *titleCart;
	UIButton *titleContact;

	UISearchBar * mySearchBarNav;

	UIVisualEffect *blurEffect;
	UIVisualEffectView *visualEffectView;

	UIToolbar * toolbar;
	UIToolbar * toolbarTextDisplay;
	UIToolbar * toolbarUnderlay;
	UIBarButtonItem *cartIcon;
	UIBarButtonItem *contactIcon;
	UIBarButtonItem *accountIcon;
	UIBarButtonItem *cescoIconGrey;
	UIBarButtonItem *cescoIconBlue;
	UIBarButtonItem *contactIconOver;
	UIBarButtonItem *notificationBox;
	UIBarButtonItem *spacer;

	UIScrollView *nativeMailerReplacingWebview;


}

@end
float originalOffsetXForMenuNav;
NSString *urlHome;
NSString * const appendUrlClient = @"/?fromClient=true";
NSString * const appendUrlClientNoForwardSlash = @"?fromClient=true";
NSString * const appendUrlClientNoForwardSlashWithAnd = @"&fromClient=true";
NSString * const backgroundColor = @"#023B66";
NSString * const searchBarDefaultText = @"What are you looking for?         ";
BOOL isVisible = NO;
BOOL patchJSForEnlargingWebView = TRUE;
BOOL firstThreadOnce = TRUE;
BarCodeScanner * vcBarcode;

int offsetYOfMainMobile = 100;
int sizeOfNavBarOnBottom = 70;
int toolbarSize = 40;
float sleepTimer = 0.75;

NSThread static * myTimer;
NSThread static * waitForPhotoThread;

float defaultAlphaForNotification = .90;

UITextField static  *fullnameForSubmitPhoto;
UITextField static *phoneNumberForSubmitPhoto;
UITextField static *subjectForSubmitPhoto;
UITextField static *sendMessageSubmitPhoto;

dispatch_semaphore_t pictureLock;

@implementation WebViewController

+(dispatch_semaphore_t) getPictureLock {
	return pictureLock;
}
-(void) threadWaitingForPicture:(id) obj {
	while(true){
		dispatch_semaphore_wait(pictureLock, DISPATCH_TIME_FOREVER);
		NSLog(@"Want to load images");
		UIImageView *imageHolder = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, 400, 400)];
		if([TakePictureController getImage] != nil){
			 dispatch_async(dispatch_get_main_queue(), ^{
			imageHolder.image = [TakePictureController getImage];
			[nativeMailerReplacingWebview addSubview:imageHolder];
			 });
			NSLog(@"Image Not Null");
		}
	}
}

+ (void)initialize
{
	static BOOL initialized = NO;
	if(!initialized)
	{

		pictureLock = dispatch_semaphore_create(0);
		fullnameForSubmitPhoto = [[UITextField alloc] initWithFrame:CGRectMake([GraphicsLib getScreenWidth]/2 - [GraphicsLib getWidthFromPercentageAsPixels:75]/2, 30, [GraphicsLib getWidthFromPercentageAsPixels:75], 40)];
		fullnameForSubmitPhoto.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
		fullnameForSubmitPhoto.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
		fullnameForSubmitPhoto.backgroundColor=[UIColor whiteColor];
		fullnameForSubmitPhoto.text=@"Enter Full Name";
		fullnameForSubmitPhoto.textAlignment = UITextAlignmentCenter;
		phoneNumberForSubmitPhoto = [[UITextField alloc] initWithFrame:CGRectMake([GraphicsLib getScreenWidth]/2 - [GraphicsLib getWidthFromPercentageAsPixels:75]/2, 65, [GraphicsLib getWidthFromPercentageAsPixels:75], 40)];
		phoneNumberForSubmitPhoto.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
		phoneNumberForSubmitPhoto.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
		phoneNumberForSubmitPhoto.backgroundColor=[UIColor whiteColor];
		phoneNumberForSubmitPhoto.text=@"Enter Phone Number";
		phoneNumberForSubmitPhoto.textAlignment = UITextAlignmentCenter;

		subjectForSubmitPhoto = [[UITextField alloc] initWithFrame:CGRectMake(45, 30, 200, 40)];
		subjectForSubmitPhoto.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
		subjectForSubmitPhoto.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
		subjectForSubmitPhoto.backgroundColor=[UIColor whiteColor];
		subjectForSubmitPhoto.text=@"Enter Subject";

		sendMessageSubmitPhoto = [[UITextField alloc] initWithFrame:CGRectMake(45, 30, 200, 40)];
		sendMessageSubmitPhoto.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
		sendMessageSubmitPhoto.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
		sendMessageSubmitPhoto.backgroundColor=[UIColor whiteColor];
		sendMessageSubmitPhoto.text=@"Enter Message details";

		initialized = YES;
		originalOffsetXForMenuNav = ([GraphicsLib getScreenWidth] * .85) * -1;
		urlHome = @"http://stage.unboundcommerce.com/gstore/cesco";

	}
}

+(BOOL) cookieValueIsNull {
	NSString * value;
	NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
	for (NSHTTPCookie *cookie in [storage cookies]) {
		if([[cookie name] isEqualToString:@"compareCescoNumbers"]){
			NSLog(@"Cookie Value %@",[cookie valueForKey:@"value"]);
			value = [cookie valueForKey:@"value"];
		}
	}
	if(value == nil || [value isEqualToString:nil] || [value isEqualToString:@""]){
		return TRUE;
	}else{
		return FALSE;
	}

}
-(void) timerListenForCookieValue: (id) obj{
	UIButton * myReferenceToBCScanner = [BarCodeScanner getExternalNotification];
	NSLog(@"THREAD START");
	while(true){
		[NSThread sleepForTimeInterval:sleepTimer];
		NSString * value;

		NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
		for (NSHTTPCookie *cookie in [storage cookies]) {
			if([[cookie name] isEqualToString:@"compareCescoNumbers"]){
				//NSLog(@"Cookie Value %@",[cookie valueForKey:@"value"]);
				value = [cookie valueForKey:@"value"];
			}
		}
		//NSLog(@"%@", value);
		if(value == nil || [value isEqualToString:@""]){
			dispatch_async(dispatch_get_main_queue(), ^{
				rotateScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = true;
				myReferenceToBCScanner.hidden = true;
			});
			//NSLog(@"True hide!");
		}else{
			dispatch_async(dispatch_get_main_queue(), ^{
				NSString *valueToken = [[NSString alloc] initWithString:value];
				NSUInteger times = [[valueToken componentsSeparatedByString:@"|"] count]-1;
				//NSLog(@"%lu Times",(unsigned long)times);

				[rotateScreenToDisplayEnlargedViewButtonOnClickNothing setTitle: [[NSString alloc] initWithFormat:@"Rotate Screen to Compare %lu item(s)", times + 1] forState:UIControlStateNormal];
				rotateScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = false;

				[myReferenceToBCScanner setTitle: [[NSString alloc] initWithFormat:@"Click Screen to Compare %lu item(s)", times + 1] forState:UIControlStateNormal];

				myReferenceToBCScanner.hidden = false;
			});
		}
	}
}

-(void) hideElement:(id) element{
	UIButton *button = element;
	[self.view sendSubviewToBack:button];
}


- (void)viewDidLoad{

	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter]
	 addObserver:self selector:@selector(orientationChanged:)
	 name:UIDeviceOrientationDidChangeNotification
	 object:[UIDevice currentDevice]];

	waitForPhotoThread = [[NSThread alloc] initWithTarget:self
												 selector:@selector(threadWaitingForPicture:)
												   object:nil];

	[waitForPhotoThread start];

	myTimer = [[NSThread alloc] initWithTarget:self
									  selector:@selector(timerListenForCookieValue:)
										object:nil];
	[myTimer start];

	NSLog(@"%@", @"Thread is listening for cookie changes");




	NSMutableURLRequest * requestForNav =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://ux.ivorscott.com/cesco/menu.html"]];
	NSString *newURL = [NSString stringWithFormat:@"%@%@", urlHome, appendUrlClientNoForwardSlash];
	NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURL]];

	toolbar = [[UIToolbar alloc] init];
	[toolbar setTranslucent:NO];
	toolbar.clipsToBounds = NO;
	toolbar.frame = CGRectMake(0, [GraphicsLib getScreenHeight] - 60, [GraphicsLib getScreenWidth], toolbarSize);

	toolbar.backgroundColor = [UIColor clearColor];
	if ([toolbar respondsToSelector:@selector(setBackgroundImage:forToolbarPosition:barMetrics:)]) {
  [toolbar setBackgroundImage:[UIImage new] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
	}
	if ([toolbar respondsToSelector:@selector(setShadowImage:forToolbarPosition:)]) {
  [toolbar setShadowImage:[UIImage new] forToolbarPosition:UIToolbarPositionAny];
	}

	toolbarTextDisplay = [[UIToolbar alloc] init];
	[toolbarTextDisplay setTranslucent:NO];
	toolbarTextDisplay.clipsToBounds = YES;
	toolbarTextDisplay.frame = CGRectMake(0, [GraphicsLib getScreenHeight] - 20, [GraphicsLib getScreenWidth], 20);



	toolbarUnderlay = [[UIToolbar alloc] init];
	[toolbarUnderlay setTranslucent:NO];
	toolbarUnderlay.frame = CGRectMake(0, [GraphicsLib getScreenHeight] - sizeOfNavBarOnBottom, [GraphicsLib getScreenWidth], sizeOfNavBarOnBottom);

	{	/*
		 Listening to keyboard, adding delegates
		 */
		NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
		[center addObserver:self selector:@selector(didShow:) name:UIKeyboardDidShowNotification object:nil];
		[center addObserver:self selector:@selector(didHide:) name:UIKeyboardWillHideNotification object:nil];
	}

	{   /*
		 Blur Effect filling object params
		 */
		blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
		disableBlurButton =[[UIButton alloc] init];
		[disableBlurButton setFrame: [GraphicsLib returnButtonObject:0 withYPosition:100 withWidth:[GraphicsLib getScreenWidth] withHeight:[GraphicsLib getScreenHeight] - 100]];
		[disableBlurButton setTitle:@"" forState:UIControlStateNormal];
		[disableBlurButton setBackgroundColor:[UIColor clearColor]];

		visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
		visualEffectView.frame = CGRectMake(0, 100, [GraphicsLib getScreenWidth],[GraphicsLib getScreenHeight] - 100);
		visualEffectView.alpha = .92;
	}

	//represents UI elements that are called when the search field is targeted.
	barCodeIcon = [GraphicsLib getImageasAsset: @"barcode_icon"  withWidth:34 withHeight :32 withX: [GraphicsLib getScreenWidth]/2 - 34/2 withY:315];

	notificationBoxImage = [GraphicsLib getImageasAsset: @"NotificationBox"  withWidth:74 withHeight :32 withX:[GraphicsLib getWidthFromPercentageAsPixels:3] withY:([GraphicsLib getScreenHeight] - 50) - 0];
	backgroundForSearchState = [GraphicsLib getImageasAsset: @"color_slice"  withWidth:[GraphicsLib getScreenWidth] withHeight :[GraphicsLib getHeightFromPercentageAsPixels:10] withX: 0 withY:-500];


	//invisible button that fires the event for the UI elements to show such as barcode icon and backdrop
	myButtonForClickEventSearch =[[UIButton alloc] init];
	[myButtonForClickEventSearch setFrame: [GraphicsLib returnButtonObject:72 withYPosition:35 withWidth:[GraphicsLib getScreenWidth] * .77 withHeight:50]];
	[myButtonForClickEventSearch setTitle:@"" forState:UIControlStateNormal];
	/*debugging
	 [myButtonForClickEventSearch setBackgroundColor:[UIColor redColor]];
	 */

	//mySearchNav
	mySearchBarNav = [[UISearchBar alloc] init];
	mySearchBarNav.frame = CGRectMake(67, 35, ([GraphicsLib getScreenWidth] - 60) * .95,50);
	[mySearchBarNav setBackgroundColor:[UIColor clearColor]];
	[mySearchBarNav setBackgroundImage:[UIImage new]];
	[mySearchBarNav setTranslucent:YES];
	mySearchBarNav.placeholder = searchBarDefaultText;
	mySearchBarNav.showsBookmarkButton = NO;
	mySearchBarNav.showsCancelButton = NO;

	//used to fire off events
	minimizeButtonState = [[UIButton alloc] init];
	[minimizeButtonState setFrame: [GraphicsLib returnButtonObject:0 withYPosition:100 withWidth:[GraphicsLib getScreenWidth] withHeight:[GraphicsLib getScreenHeight] - 100]];
	[minimizeButtonState setTitle:@"" forState:UIControlStateNormal];

	myButtonForMenu = [[UIButton alloc] init];
	[myButtonForMenu setFrame:[GraphicsLib returnButtonObject:10 withYPosition:32 withWidth:53 withHeight:65]];
	[myButtonForMenu setTitle:@"" forState:UIControlStateNormal];

	myBackgroundImage = [GraphicsLib getImageAsBackground:@"color_slice"];
	mySepBarImage = [GraphicsLib getImageasAsset: @"sep_bar"  withWidth:26 * .8 withHeight :76 * .8 withX: 55 withY:31];


	menuLabel = [[UILabel alloc] init];
	[menuLabel setFrame:CGRectMake(14, 52, 50,50)];
	menuLabel.backgroundColor=[UIColor clearColor];
	menuLabel.textColor=[UIColor whiteColor];
	menuLabel.userInteractionEnabled=NO;
	menuLabel.text= @"Menu";
	[menuLabel setFont:[UIFont fontWithName:@"HelveticaNeue" size:16]];

	textForNotificationBox = [[UILabel alloc] init];
	textForNotificationBox.numberOfLines = 2;
	[textForNotificationBox setFrame:CGRectMake([GraphicsLib getWidthFromPercentageAsPixels:4.5], [GraphicsLib getScreenHeight] - (115 - 25), 100,115)];
	textForNotificationBox.backgroundColor=[UIColor clearColor];
	textForNotificationBox.textColor=[GraphicsLib colorWithHexString:backgroundColor];
	textForNotificationBox.userInteractionEnabled=NO;
	NSString * resourceText = [[NSString alloc] initWithString:[Resource getIconOneText]];
	textForNotificationBox.text= resourceText;
	[textForNotificationBox setFont:[UIFont fontWithName:@"HelveticaNeue" size:12]];


	mySearchToggleIcon = [GraphicsLib getImageasAsset:@"Menu.png" withWidth:48*.8 withHeight:32*.8 withX:15 withY:38];

	mainWebViewMobileSite = [[UIWebView alloc]initWithFrame:CGRectMake(0, offsetYOfMainMobile, [GraphicsLib getScreenWidth],(-1 * (offsetYOfMainMobile + sizeOfNavBarOnBottom)) + [GraphicsLib getScreenHeight])];
	mainWebViewMobileSite.scrollView.bounces = NO;


	[mainWebViewMobileSite loadRequest:request];

	nativeMailerReplacingWebview = [[UIScrollView alloc] initWithFrame:CGRectMake(0, offsetYOfMainMobile, [GraphicsLib getScreenWidth], (-1 * (offsetYOfMainMobile + sizeOfNavBarOnBottom)) + [GraphicsLib getScreenHeight])];
	[nativeMailerReplacingWebview setBackgroundColor:[UIColor whiteColor]];

	[nativeMailerReplacingWebview addSubview:fullnameForSubmitPhoto];
	[nativeMailerReplacingWebview addSubview:phoneNumberForSubmitPhoto];


	webViewNavBar = [[UIWebView alloc]initWithFrame:CGRectMake(originalOffsetXForMenuNav, 0, [GraphicsLib getScreenWidth] * .85,[GraphicsLib getScreenHeight])];
	webViewNavBar.scrollView.bounces = NO;

	[webViewNavBar loadRequest:requestForNav];
	[webViewNavBar scrollView].contentSize = CGSizeMake(0, 900);
	[webViewNavBar setBackgroundColor:[GraphicsLib colorWithHexString:backgroundColor]];

	rotateScreenToDisplayEnlargedViewButtonOnClickNothing = [UIButton buttonWithType:UIButtonTypeRoundedRect] ;
	rotateScreenToDisplayEnlargedViewButtonOnClickNothing.frame = CGRectMake(0, 0, [GraphicsLib getScreenWidth], [GraphicsLib getHeightFromPercentageAsPixels:10]);
	[rotateScreenToDisplayEnlargedViewButtonOnClickNothing addTarget:self
															  action:@selector(hideElement:)
													forControlEvents:UIControlEventTouchUpInside];
	[GraphicsLib setButtonToBottom:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];
	[GraphicsLib setButtonAtXCenter:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];
	[GraphicsLib setButtonAtYPosition:rotateScreenToDisplayEnlargedViewButtonOnClickNothing withNewAtY:rotateScreenToDisplayEnlargedViewButtonOnClickNothing.frame.origin.y - sizeOfNavBarOnBottom];
	[rotateScreenToDisplayEnlargedViewButtonOnClickNothing setBackgroundColor:[UIColor orangeColor]];
	[rotateScreenToDisplayEnlargedViewButtonOnClickNothing setTitle:@"Rotate Screen to compare items(s)" forState:(UIControlStateNormal)];
	rotateScreenToDisplayEnlargedViewButtonOnClickNothing.alpha = defaultAlphaForNotification;

	/*
	 Adding views to self view as subviews
	 */
	[self.view addSubview:myBackgroundImage];
	[self.view addSubview:minimizeButtonState];
	[self.view addSubview:mainWebViewMobileSite];
	[self.view addSubview:nativeMailerReplacingWebview];
	loadingView = [GraphicsLib returnLoadingViewInCenter:80 withHeight: 80];
	[self.view addSubview:loadingView];

	[self.view addSubview:mySearchToggleIcon];
	[self.view addSubview:menuLabel];
	[self.view addSubview:mySepBarImage];
	[self.view addSubview:myButtonForMenu];
	[self.view addSubview:mySearchBarNav];
	[self.view addSubview:visualEffectView];
	[self.view addSubview:disableBlurButton];
	[self.view addSubview:myButtonForClickEventSearch];
	[self.view addSubview:backgroundForSearchState];
	[self.view addSubview:barCodeIcon];
	[self.view addSubview:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];
	[self.view addSubview:webViewNavBar];




	[disableBlurButton addTarget:self action:@selector(onClickEndEditing:) forControlEvents:UIControlEventTouchDown];
	[myButtonForClickEventSearch addTarget:self action:@selector(onClickShowBar:) forControlEvents:UIControlEventTouchUpInside];
	[minimizeButtonState addTarget:self action:@selector(onClickMinimize:) forControlEvents:UIControlEventTouchDown];
	[myButtonForMenu addTarget:self action:@selector(onReleaseToNavView:) forControlEvents:UIControlEventTouchUpInside];
	UITapGestureRecognizer *singleFingerTapBarCode =
	[[UITapGestureRecognizer alloc] initWithTarget:self
											action:@selector(handleSingleTap:)];
	[barCodeIcon addGestureRecognizer:singleFingerTapBarCode];


	notificationBox=[[UIBarButtonItem alloc]initWithImage: [[UIImage imageNamed:@"Spacer.gif"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
													style:UIBarButtonItemStylePlain
												   target:self
												   action:@selector(doNothing:)];


	cartIcon = [[UIBarButtonItem alloc] initWithImage: [[UIImage imageNamed:@"CartGrey"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
												style:UIBarButtonItemStylePlain
											   target:self
											   action:@selector(doNothing:)];

	contactIcon=[[UIBarButtonItem alloc]initWithImage: [[UIImage imageNamed:@"ContactGrey"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
												style:UIBarButtonItemStylePlain
											   target:self
											   action:@selector(showNewWebPageContact:)];
	contactIconOver =[[UIBarButtonItem alloc]initWithImage: [[UIImage imageNamed:@"ContactBlue"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
													 style:UIBarButtonItemStylePlain
													target:self
													action:@selector(doNothing:)];

	accountIcon=[[UIBarButtonItem alloc]initWithImage: [[UIImage imageNamed:@"AccountGrey"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
												style:UIBarButtonItemStylePlain
											   target:self
											   action:@selector(doNothing:)];

	cescoIconGrey=[[UIBarButtonItem alloc]initWithImage: [[UIImage imageNamed:@"CescoGrey"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
												  style:UIBarButtonItemStylePlain
												 target:self
												 action:@selector(singleFingerTapCescoGreyIconChange:)];
	cescoIconBlue=[[UIBarButtonItem alloc]initWithImage: [[UIImage imageNamed:@"CescoBlue"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
												  style:UIBarButtonItemStylePlain
												 target:self
												 action:@selector(singleFingerTapCescoGreyIconChange:)];



	int const SCALE_FOR_NARBAR_AS_PERCENT = 16;
	spacer=[[UIBarButtonItem alloc] init];
	spacer.title=@"";
	spacer.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];

	accountIcon.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];
	cescoIconBlue.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];
	cescoIconGrey.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];

	cartIcon.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];
	contactIcon.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];
	contactIconOver.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];


	//todo : refactor
	titleAccount = [UIButton buttonWithType:UIButtonTypeCustom];
	[titleAccount setTitle:@"Sign in" forState:UIControlStateNormal];
	titleAccount.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
	[titleAccount setTitleColor:[UIColor grayColor] forState:(UIControlStateNormal)];
	[titleAccount.layer setBorderColor: [[UIColor grayColor] CGColor]];
	titleAccount.frame=CGRectMake(0.0, 0, 32, 20);

	titleContact = [UIButton buttonWithType:UIButtonTypeCustom];
	[titleContact setTitle:@"Contact" forState:UIControlStateNormal];
	titleContact.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
	[titleContact setTitleColor:[UIColor grayColor] forState:(UIControlStateNormal)];
	[titleContact.layer setBorderColor: [[UIColor grayColor] CGColor]];
	titleContact.frame=CGRectMake(0.0, 0, 32, 20);

	titleCart = [UIButton buttonWithType:UIButtonTypeCustom];
	[titleCart setTitle:@"Cart " forState:UIControlStateNormal];
	titleCart.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:12.0f];
	[titleCart setTitleColor:[UIColor grayColor] forState:(UIControlStateNormal)];
	[titleCart.layer setBorderColor: [[UIColor grayColor] CGColor]];
	titleCart.frame=CGRectMake(0.0, 0, 32, 20);




	UIBarButtonItem* titleAccountObj = [[UIBarButtonItem alloc] initWithCustomView:titleAccount];
	UIBarButtonItem* titleContactObj = [[UIBarButtonItem alloc] initWithCustomView:titleContact];
	UIBarButtonItem* titleCartObj = [[UIBarButtonItem alloc] initWithCustomView:titleCart];
	titleAccountObj.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];
	titleContactObj.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];
	titleCartObj.width = [GraphicsLib getWidthFromPercentageAsPixels:SCALE_FOR_NARBAR_AS_PERCENT];

	[toolbarTextDisplay setItems:[[NSArray alloc] initWithObjects:notificationBox, titleAccountObj,spacer, titleContactObj, titleCartObj, nil]];
	[toolbar setItems:[[NSArray alloc] initWithObjects:notificationBox, accountIcon,cescoIconGrey, contactIcon, cartIcon, nil]];



	[self.view addSubview:toolbarUnderlay];
	[self.view addSubview:toolbarTextDisplay];
	[self.view addSubview:toolbar];
	[self.view addSubview:textForNotificationBox];
	[self.view addSubview:notificationBoxImage];

	/*
	 Hide onload UI elements
	 */
	nativeMailerReplacingWebview.hidden = true;
	rotateScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = true;
	mainWebViewMobileSite.hidden = true;
	barCodeIcon.hidden = TRUE;
	backgroundForSearchState.hidden = TRUE;
	disableBlurButton.hidden = true;
	visualEffectView.hidden = true;
	webViewNavBar.hidden = TRUE;

	[self.view setBackgroundColor:[UIColor whiteColor]];
	[mainWebViewMobileSite setBackgroundColor:[UIColor whiteColor]];
	[super viewDidLoad];
	/*
	 Delegates below
	 */
	[mainWebViewMobileSite setDelegate:self];
	[webViewNavBar setDelegate:self];
	mySearchBarNav.delegate = self;
}
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return nil;
}

- (void) orientationChanged:(NSNotification *)note{
	UIDevice * device = note.object;
	CGRect boundsLandscape = mainWebViewMobileSite.bounds;

	NSString *html = [mainWebViewMobileSite stringByEvaluatingJavaScriptFromString:@"document.documentElement.innerHTML"];

	NSString * value;
	NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
	for (NSHTTPCookie *cookie in [storage cookies]) {
		if([[cookie name] isEqualToString:@"compareCescoNumbers"]){
			NSLog(@"Cookie Value %@",[cookie valueForKey:@"value"]);
			value = [cookie valueForKey:@"value"];
		}
	}

	NSLog(@"%@ Orientation Cookie Value", value);
	[[NSUserDefaults standardUserDefaults] synchronize];

	if( [value length] == 0 ){
		NSLog(@"%@ Value length 0", value);
	}

	switch(device.orientation)
	{
		{case UIDeviceOrientationPortrait:
			NSLog(@"(RE-RENDER UI VIEW ELEMENTS) ---Orientation Portrait--- (RE-RENDER UI VIEW ELEMENTS)");
			//[mainWebViewMobileSite stringByEvaluatingJavaScriptFromString:@"goCompare()"];
			mainWebViewMobileSite.transform = CGAffineTransformMakeRotation(0);
			boundsLandscape.size.height = [GraphicsLib getScreenHeight] - (offsetYOfMainMobile + sizeOfNavBarOnBottom);
			boundsLandscape.size.width = [GraphicsLib getScreenWidth];
			mainWebViewMobileSite.bounds = boundsLandscape;


			mainWebViewMobileSite.scalesPageToFit = YES;
			mainWebViewMobileSite.contentMode = UIViewContentModeScaleAspectFit;

			[self.view bringSubviewToFront:toolbarUnderlay];
			[self.view bringSubviewToFront:toolbarTextDisplay];
			[self.view bringSubviewToFront:toolbar];
			[self.view bringSubviewToFront:textForNotificationBox];
			[self.view bringSubviewToFront:notificationBoxImage];

			[self.view bringSubviewToFront:backgroundForSearchState];
			[self.view bringSubviewToFront:barCodeIcon];
			[self.view bringSubviewToFront:webViewNavBar];
			[self.view setBackgroundColor: [GraphicsLib colorWithHexString:backgroundColor]];
			[self.view bringSubviewToFront:loadingView];
			break;
		}
		{case UIDeviceOrientationLandscapeLeft:
			if(([mainWebViewMobileSite.request.URL.absoluteString containsString:@"category"] && [html containsString:@"Compare"] && ![value isEqualToString:@""] && !(value != nil) && ![value isEqualToString:[NSNull null]] && [value length] > 0) || ([mainWebViewMobileSite.request.URL.absoluteString containsString:@"productCompare?compareCescoNumbers"]) ){
				NSLog(@"Orientation Landscape LEFT");
				[mainWebViewMobileSite stringByEvaluatingJavaScriptFromString:@"goCompare()"];
				CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
				mainWebViewMobileSite.transform = CGAffineTransformRotate(transform, M_PI_2 );

				boundsLandscape.size.height = [GraphicsLib getScreenWidth];
				boundsLandscape.size.width = [GraphicsLib getScreenHeight] - 20;
				mainWebViewMobileSite.bounds = boundsLandscape;

				mainWebViewMobileSite.scalesPageToFit = YES;
				mainWebViewMobileSite.contentMode = UIViewContentModeScaleAspectFit;

				[self.view sendSubviewToBack:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];

				[self.view bringSubviewToFront:mainWebViewMobileSite];
				[self.view bringSubviewToFront:loadingView];

				[self.view setBackgroundColor: [UIColor whiteColor]];
				break;
			}

		}
			break;
		{case UIDeviceOrientationLandscapeRight:
			if(([mainWebViewMobileSite.request.URL.absoluteString containsString:@"category"] && [html containsString:@"Compare"] && ![value isEqualToString:@""] && (value != nil) && ![value isEqualToString:[NSNull null]] && [value length] > 0) || ([mainWebViewMobileSite.request.URL.absoluteString containsString:@"productCompare?compareCescoNumbers"])){
				NSLog(@"Orientation Landscape RIGHT");
				[mainWebViewMobileSite stringByEvaluatingJavaScriptFromString:@"goCompare()"];
				CGAffineTransform transform = CGAffineTransformMakeTranslation(0, 0);
				mainWebViewMobileSite.transform = CGAffineTransformRotate(transform, M_PI + M_PI_2);
				boundsLandscape.size.height = [GraphicsLib getScreenWidth];
				boundsLandscape.size.width = [GraphicsLib getScreenHeight] - 20;
				mainWebViewMobileSite.bounds = boundsLandscape;

				mainWebViewMobileSite.scalesPageToFit = YES;
				mainWebViewMobileSite.contentMode = UIViewContentModeScaleAspectFit;

				[self.view sendSubviewToBack:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];


				[self.view bringSubviewToFront:mainWebViewMobileSite];
				[self.view bringSubviewToFront:loadingView];
				[self.view setBackgroundColor: [UIColor whiteColor]];
			}
		}
		default:
			break;
	};
}

- (void)didShow:(NSNotification*)notification{
	if([mySearchBarNav isFirstResponder]){

		CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;


		int const SIZE_OF_BAR = 150;
		int const OFFSET = 6;

		NSLog(@"%f <- Height of screen", height);
		CGRect backgroundFrame = backgroundForSearchState.frame;
		backgroundFrame.size.height = SIZE_OF_BAR;
		backgroundFrame.origin.y = [GraphicsLib getScreenHeight] - height - SIZE_OF_BAR + 100;


		CGRect iconFrame = barCodeIcon.frame;
		iconFrame.origin.y = [GraphicsLib getScreenHeight] - height - (32 + OFFSET);


		dispatch_async(dispatch_get_main_queue(), ^{
			backgroundForSearchState.contentMode = UIViewContentModeScaleAspectFill;
			backgroundForSearchState.clipsToBounds = YES;

			barCodeIcon.contentMode = UIViewContentModeScaleAspectFill;
			barCodeIcon.clipsToBounds = YES;

			[backgroundForSearchState setFrame:backgroundFrame];
			[barCodeIcon setFrame:iconFrame];

		});

		barCodeIcon.hidden = FALSE;
		backgroundForSearchState.hidden = FALSE;
		isVisible = YES;
		disableBlurButton.hidden = false;
		visualEffectView.hidden = false;
	}

}

- (void)didHide:(NSNotification*)notification{
	disableBlurButton.hidden = true;
	visualEffectView.hidden = true;
	isVisible = NO;
	barCodeIcon.hidden = TRUE;
	backgroundForSearchState.hidden = TRUE;
	myButtonForClickEventSearch.hidden = FALSE;

}

-(void) setBaseUrl:(NSString * )myNewBase{
	urlHome = [[NSString alloc]initWithString:myNewBase];
}

- (void)viewDidAppear:(BOOL) animated{
	NSLog(@"view appeared for WebViewController");
	[super viewDidAppear:YES];
}

-(void) doNothing: (id)messenger{

}

-(void)showNewWebPageContact:(id)messeger{
	[self setBaseUrl:@"http://stage.unboundcommerce.com/gstore/cesco/contents/aboutUs/contactUs.jsp"] ;
	[mainWebViewMobileSite loadRequest: [NSMutableURLRequest requestWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", urlHome, appendUrlClientNoForwardSlash]]]];
	mainWebViewMobileSite.hidden = true;
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
	vcBarcode = [[BarCodeScanner alloc] init];
	UIViewController *presentingVC = [self presentingViewController];
	[BarCodeScanner setbarCodeScannerButtonStatusYES];
	[self dismissViewControllerAnimated:NO completion:
	 ^{
		 [presentingVC presentViewController:vcBarcode animated:NO completion:nil];
	 }];
}


- (void)singleFingerTapCescoGreyIconChange:(UITapGestureRecognizer *)recognizer {
	[self setBaseUrl:@"http://stage.unboundcommerce.com/gstore/cesco"] ;
	mainWebViewMobileSite.hidden = true;
	[mainWebViewMobileSite loadRequest: [NSMutableURLRequest requestWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@%@", urlHome, appendUrlClientNoForwardSlash]]]];
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
	NSLog(@"CLICKED ENTER SUBMIT");
	NSString * searchBarText = searchBar.text;
	searchBarText = [searchBarText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

	[BarCodeScanner setDefaultUrlLoad:searchBarText];
	vcBarcode = [[BarCodeScanner alloc] init];
	UIViewController *presentingVC = [self presentingViewController];
	[self dismissViewControllerAnimated:NO completion:
	 ^{
		 [presentingVC presentViewController:vcBarcode animated:NO completion:nil];
	 }];

}


-(void)onClickShowBar:(id)sender{
	NSLog(@"%@",@"Clicked on event for search bar");
	[mySearchBarNav becomeFirstResponder];
	myButtonForClickEventSearch.hidden = TRUE;
	disableBlurButton.hidden = false;
	visualEffectView.hidden = false;
}

-(void)onClickMinimize:(id)sender{
	toolbarTextDisplay.hidden = false;
	notificationBoxImage.hidden = false;
	textForNotificationBox.hidden = false;
	webViewNavBar.hidden = TRUE;
	[super.view sendSubviewToBack:minimizeButtonState];
	mainWebViewMobileSite.alpha=1.0f;
	toolbar.alpha = 1.0f;
	toolbarUnderlay.alpha = 1.0f;
	CGRect newFrame = webViewNavBar.frame;
	newFrame.origin.x = originalOffsetXForMenuNav;
	webViewNavBar.frame = newFrame;
	rotateScreenToDisplayEnlargedViewButtonOnClickNothing.alpha = defaultAlphaForNotification;
	myButtonForClickEventSearch.hidden = false;
	[mySearchBarNav setUserInteractionEnabled:NO];

}

-(void)onReleaseToNavView:(id)sender{
	toolbarTextDisplay.hidden = true;
	notificationBoxImage.hidden = true;
	textForNotificationBox.hidden = true;
	[mySearchBarNav setUserInteractionEnabled:NO];
	webViewNavBar.hidden = FALSE;
	[super.view bringSubviewToFront:minimizeButtonState];
	[super.view bringSubviewToFront:webViewNavBar];
	mainWebViewMobileSite.alpha=0.35f;
	toolbar.alpha = 0.0f;
	rotateScreenToDisplayEnlargedViewButtonOnClickNothing.alpha = 0.0f;
	toolbarUnderlay.alpha = .35f;
	[self.view endEditing:YES];
	disableBlurButton.hidden = true;
	visualEffectView.hidden = true;
	myButtonForClickEventSearch.hidden = true;

	CGRect newFrame = webViewNavBar.frame;
	newFrame.origin.x = 0;

	[UIView animateWithDuration:.7
						  delay: 0.0
						options: UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 webViewNavBar.frame = newFrame;   // move
					 }
					 completion:^(BOOL finished){
						 NSLog(@"completion block of animation");
					 }];
}

-(NSString * ) removeLastSlashIfExists: (NSString*) string{
	if ([string length] > 2) {
		if( [string characterAtIndex:[string length] - 1] == '/'){
			return [string substringToIndex:[string length] - 1];
		}else{
			return [string substringToIndex:[string length]];
		}
	} else {
		return @"";
	}
}

-(void)onClickEndEditing:(id)sender{
	[self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	[webViewNavBar reload];
	[mainWebViewMobileSite reload];

	// Dispose of any resources that can be recreated.
}

//TODO: Bug where sometimes the request will not append
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	NSString *myURLGlobal = [request.URL absoluteString];

	NSLog(@"%@ Redirect Url <-", myURLGlobal);


	if([myURLGlobal containsString:appendUrlClientNoForwardSlashWithAnd]){
		return YES;
	}

	nativeMailerReplacingWebview.hidden = true;
	mainWebViewMobileSite.hidden = false;
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		NSURL *url = request.URL;
		NSString *scheme = [url scheme];
		NSString * newURL;
		if([[url absoluteString] containsString:@"#take_photo"]){
			NSLog(@"%@",@"Start photo logic.");
			nativeMailerReplacingWebview.hidden = false;
			mainWebViewMobileSite.hidden = true;
			TakePictureController * vc = [[TakePictureController alloc] init];
			[self presentViewController:vc animated: NO completion:^{

			}];






			return NO;
		}
	}


	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		NSURL *url = request.URL;
		NSString *scheme = [url scheme];
		NSString * newURL;
		if([[url absoluteString] containsString:@"&view=grid"]){
			NSLog(@"%@",@"Statement hit");
			newURL = [NSString stringWithFormat:@"%@%@", [NSString stringWithFormat:@"%@", [url absoluteString]], appendUrlClientNoForwardSlashWithAnd];
			[mainWebViewMobileSite loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURL]]];
			return NO;
		}
	}

	if([myURLGlobal containsString:@"category"] && [myURLGlobal containsString:@"compareReturnUrl"] && ![myURLGlobal containsString:@"www.rating-system.com"] && ([myURLGlobal containsString:@"b2c"] || [myURLGlobal containsString:@"b2b"])){
		NSString * newURLGlobal = [NSString stringWithFormat:@"%@%@", myURLGlobal, appendUrlClientNoForwardSlashWithAnd];
		if([myURLGlobal containsString:appendUrlClientNoForwardSlashWithAnd]){
			return YES;
		}else{
			[mainWebViewMobileSite loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURLGlobal]]];
			return NO;
		}
	}

	if([myURLGlobal containsString:@"category"] && ![myURLGlobal containsString:@"www.rating-system.com"] && ([myURLGlobal containsString:@"b2c"] || [myURLGlobal containsString:@"b2b"])){
		NSString * newURLGlobal = [NSString stringWithFormat:@"%@%@", myURLGlobal, appendUrlClientNoForwardSlash];
		if([myURLGlobal containsString:appendUrlClientNoForwardSlash]){
			return YES;
		}else{
			[mainWebViewMobileSite loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURLGlobal]]];
			return NO;
		}

	}

	//Load these requests (compare)
	if([myURLGlobal containsString:@"productCompare"] && [myURLGlobal containsString:appendUrlClientNoForwardSlashWithAnd]){
		return YES;
	}

	//do not load these requests (compare)
	if([myURLGlobal containsString:@"productCompare"] && ![myURLGlobal containsString:@"www.rating-system"] && ![myURLGlobal containsString:appendUrlClientNoForwardSlashWithAnd]){

		myURLGlobal = [self removeLastSlashIfExists:myURLGlobal];
		//then navigate to new web view page and decline first request.
		NSString * newURLGlobal = [NSString stringWithFormat:@"%@%@", myURLGlobal, appendUrlClientNoForwardSlashWithAnd];
		[mainWebViewMobileSite loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURLGlobal]]];
		mainWebViewMobileSite.scrollView.delegate = self;
		return NO;
	}

	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		NSURL *url = request.URL;
		NSString *scheme = [url scheme];
		NSString * newURL;
		if([webViewNavBar isEqual:webView]){
			[self onClickMinimize:nil];
		}

		if (![scheme containsString:appendUrlClient]) {

			if([[url absoluteString] containsString:@".jsp?"]){
				NSLog(@"%@",@"Statement hit");
				newURL = [NSString stringWithFormat:@"%@%@", [NSString stringWithFormat:@"%@", [[url absoluteString] substringToIndex:[[url absoluteString] length] - 1]], appendUrlClientNoForwardSlashWithAnd];
				[mainWebViewMobileSite loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURL]]];
				return NO;
			}



			if([webView isEqual:webViewNavBar]){

				newURL = [NSString stringWithFormat:@"%@%@", [NSString stringWithFormat:@"%@", url], appendUrlClientNoForwardSlash];
				[mainWebViewMobileSite loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURL]]];
			}
			else{
				newURL = [NSString stringWithFormat:@"%@%@", url, appendUrlClientNoForwardSlash];
				[webView loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURL]]];
			}



			NSLog(@"MY DEBUG URL %@",scheme);
			return NO; // don't let the webview process it.
		}
	}
	return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
	loadingView.hidden = false;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
	dispatch_async(dispatch_get_main_queue(), ^{
		mainWebViewMobileSite.scrollView.contentInset = UIEdgeInsetsMake(0.0,0.0,0.0,0.0);
	});

	[(UIScrollView *)[webView.subviews objectAtIndex:0] setScrollsToTop:YES];
	loadingView.hidden = true;

	NSString *currentURL = webView.request.URL.absoluteString;
	NSLog(@"currentUrl%@", currentURL);

	NSString *html = [mainWebViewMobileSite stringByEvaluatingJavaScriptFromString:@"document.documentElement.innerHTML"];
	if([mainWebViewMobileSite.request.URL.absoluteString containsString:@"category"] && [html containsString:@"Compare"]){
		[self.view bringSubviewToFront:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];
		mainWebViewMobileSite.scrollView.contentInset = UIEdgeInsetsMake(0.0,0.0,[GraphicsLib getHeightFromPercentageAsPixels:10],0.0);
		if(![WebViewController cookieValueIsNull]){
			rotateScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = false;
		}else{
			rotateScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = true;
		}
	}else{
		rotateScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = true;
		[self.view sendSubviewToBack:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];
	}

	if([currentURL containsString:@"compareReturnUrl"] || [currentURL containsString:@"productCompare?compareCescoNumbers"]){
		mainWebViewMobileSite.scrollView.delegate = self;
		[self.view sendSubviewToBack:rotateScreenToDisplayEnlargedViewButtonOnClickNothing];

		[mainWebViewMobileSite stringByEvaluatingJavaScriptFromString:
		 [NSString stringWithFormat:
		  @"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ",
		  (int)mainWebViewMobileSite.frame.size.width]];
		[mainWebViewMobileSite stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom=.5;"];
	}

	//contact url reached, change button over
	if([currentURL isEqualToString:@"http://stage.unboundcommerce.com/gstore/cesco/contents/aboutUs/contactUs.jsp?fromClient=true"]){



		CGRect newFrame = mainWebViewMobileSite.frame;
		newFrame.origin.y += 500;
		mainWebViewMobileSite.frame = newFrame;
		CGRect newFrameAnimateTo = mainWebViewMobileSite.frame;
		newFrameAnimateTo.origin.y = 100;
		//done loading -> animate
		mainWebViewMobileSite.hidden = false;
		[UIView animateWithDuration:.7
							  delay: 0.0
							options: UIViewAnimationOptionCurveEaseIn
						 animations:^{
							 mainWebViewMobileSite.frame = newFrameAnimateTo;   // move
						 }
						 completion:^(BOOL finished){
							 [toolbar setItems:[[NSArray alloc] initWithObjects:notificationBox, accountIcon,cescoIconGrey, contactIconOver, cartIcon, nil]];

						 }];

	}else if([currentURL isEqualToString:@"http://stage.unboundcommerce.com/gstore/cesco?fromClient=true"]){

		CGRect newFrame = mainWebViewMobileSite.frame;
		newFrame.origin.y += 500;
		mainWebViewMobileSite.frame = newFrame;
		CGRect newFrameAnimateTo = mainWebViewMobileSite.frame;
		newFrameAnimateTo.origin.y = 100;
		//done loading -> animate
		[UIView animateWithDuration:.7
							  delay: 0.0
							options: UIViewAnimationOptionCurveEaseIn
						 animations:^{
							 mainWebViewMobileSite.frame = newFrameAnimateTo;   // move
						 }
						 completion:^(BOOL finished){
							 mainWebViewMobileSite.hidden = false;
							 [toolbar setItems:[[NSArray alloc] initWithObjects:notificationBox, accountIcon,cescoIconBlue, contactIcon, cartIcon, nil]];

						 }];
	}else{
		dispatch_async(dispatch_get_main_queue(), ^{
			[toolbar setItems:[[NSArray alloc] initWithObjects:notificationBox, accountIcon,cescoIconGrey, contactIcon, cartIcon, nil]];
		});
	}
	
}


- (NSString*) urlRequestToString:(NSURLRequest*)urlRequest{
	NSString *requestPath = [[urlRequest URL] absoluteString];
	return requestPath;
}

- (NSUInteger)supportedInterfaceOrientations{
	return UIInterfaceOrientationMaskPortrait;
}

@end