//
//  BarCodeScanner.m
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/12/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import "BarCodeScanner.h"

@interface BarCodeScanner (){
	AVCaptureSession * session;
	AVCaptureDevice * device;
	AVCaptureDeviceInput * input;
	AVCaptureMetadataOutput * output;
	AVCaptureVideoPreviewLayer * prevLayer;

	UIView* loadingView;
	UIView *myScannerBox;
	UIView* backgroundForSearchState;
	UIView* backgroundForSearchStateBottom;

	UISearchBar * mySearchBarNav;

	UIButton *submitSearchButton;
	UIButton *backButtonForActivity;
	UIButton *scannerButton;
}
@end
UIWebView *webViewForScannerSearch;
WebViewController * vcWebViewController;
BOOL IS_APPENDING;

NSString * const urlSearch = @"http://stage.unboundcommerce.com/gstore/cesco/search/N=0&Ntk=defaultSearch&Ntx=mode+matchallpartial&Nty=1&Ntt=";
NSString * const appendUrlClientSearch = @"?fromClient=true";

NSString * defaultLoadPageUrl = @"http://stage.unboundcommerce.com/gstore/cesco/search/N=0&Ntk=defaultSearch&Ntx=mode+matchallpartial&Nty=1&Ntt=";

UIButton * clickScreenToDisplayEnlargedViewButtonOnClickNothing;
float defaultAlphaForClickNotification = .90;

int backgroundForSearchStateHeight = 80;
int backgroundForSearchStateHeightBottom = 80;


//class variables (C/obj C Style methodization)
@implementation BarCodeScanner
UILabel *content;
UILabel *type;
NSString * const searchBarDefaultBCText = @"Search here!    ";
BOOL barCodeScannerButtonStatus = NO;

+(void) setbarCodeScannerButtonStatusYES{
	barCodeScannerButtonStatus = YES;
}

+(void) setbarCodeScannerButtonStatusNO{
	barCodeScannerButtonStatus = NO;
}

+(UIButton *) getExternalNotification{
	return clickScreenToDisplayEnlargedViewButtonOnClickNothing;
}
+(void)initialize{
	vcWebViewController = [[WebViewController alloc] init];
	IS_APPENDING = TRUE;

	clickScreenToDisplayEnlargedViewButtonOnClickNothing = [UIButton buttonWithType:UIButtonTypeRoundedRect] ;
	clickScreenToDisplayEnlargedViewButtonOnClickNothing.frame = CGRectMake(0, 0, [GraphicsLib getScreenWidth], [GraphicsLib getHeightFromPercentageAsPixels:10]);
	[GraphicsLib setButtonToBottom:clickScreenToDisplayEnlargedViewButtonOnClickNothing];
	[GraphicsLib setButtonAtXCenter:clickScreenToDisplayEnlargedViewButtonOnClickNothing];
	[GraphicsLib setButtonAtYPosition:clickScreenToDisplayEnlargedViewButtonOnClickNothing withNewAtY:[GraphicsLib getScreenHeight] - (80 + [GraphicsLib getHeightFromPercentageAsPixels:10])];
	
	[clickScreenToDisplayEnlargedViewButtonOnClickNothing setBackgroundColor:[UIColor orangeColor]];
	[clickScreenToDisplayEnlargedViewButtonOnClickNothing setTitle:@"Click Screen to compare items(s)" forState:(UIControlStateNormal)];
	clickScreenToDisplayEnlargedViewButtonOnClickNothing.alpha = defaultAlphaForClickNotification;
	[clickScreenToDisplayEnlargedViewButtonOnClickNothing addTarget:self
															 action:@selector(clickElement:)
												   forControlEvents:UIControlEventTouchUpInside];
	
}

- (void)viewDidLoad {
	

	backButtonForActivity =[[UIButton alloc] init];
	[backButtonForActivity setFrame: [GraphicsLib returnButtonObject:0 withYPosition:0 withWidth:60 withHeight:80]];
	[backButtonForActivity setTitle:@"< Back" forState:UIControlStateNormal];
	[GraphicsLib setButtonToBottom:backButtonForActivity];
	[GraphicsLib setButtonAtXPosition:backButtonForActivity withNewAtX:10];

	submitSearchButton = [[UIButton alloc] init];
	[GraphicsLib setButtonStyleTwoForBottomMostButton:submitSearchButton withText:@"Search"];
	[GraphicsLib setButtonAtXCenter:submitSearchButton];


	backgroundForSearchState = [GraphicsLib getImageasAsset: @"color_slice"  withWidth:[GraphicsLib getScreenWidth] withHeight :backgroundForSearchStateHeight withX: 0 withY:0];

	backgroundForSearchStateBottom =[GraphicsLib getImageasAsset: @"color_slice"  withWidth:[GraphicsLib getScreenWidth] withHeight :backgroundForSearchStateHeightBottom withX: 0 withY:[GraphicsLib getScreenHeight] - 80];

	mySearchBarNav = [[UISearchBar alloc] init];
	mySearchBarNav.frame = CGRectMake(0, 20, [GraphicsLib getScreenWidth],50);
	[mySearchBarNav setBackgroundColor:[UIColor clearColor]];
	[mySearchBarNav setBackgroundImage:[UIImage new]];
	[mySearchBarNav setTranslucent:YES];
	mySearchBarNav.placeholder = searchBarDefaultBCText;

	mySearchBarNav.showsBookmarkButton = NO;
	mySearchBarNav.showsCancelButton = NO;

	myScannerBox  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [GraphicsLib getHeightFromPercentageAsPixels:30], [GraphicsLib getHeightFromPercentageAsPixels:30])];
	[myScannerBox setAlpha:.30];
	myScannerBox.backgroundColor = [UIColor greenColor];
	myScannerBox.center = self.view.center;




	webViewForScannerSearch = [[UIWebView alloc]initWithFrame:CGRectMake(0, backgroundForSearchStateHeight, [GraphicsLib getScreenWidth],[GraphicsLib getScreenHeight] - backgroundForSearchStateHeight - backgroundForSearchStateHeightBottom)];
	webViewForScannerSearch.scrollView.bounces = NO;
	webViewForScannerSearch.scalesPageToFit = NO;

	[webViewForScannerSearch.scrollView setShowsHorizontalScrollIndicator:NO];
	[webViewForScannerSearch.scrollView setShowsVerticalScrollIndicator:NO];


	if(IS_APPENDING){
		NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",defaultLoadPageUrl, appendUrlClientSearch]]];
		NSLog(@"Default second load url%@", request);
		if(![defaultLoadPageUrl isEqualToString:urlSearch]){
			[webViewForScannerSearch loadRequest:request];
		}

	}


	type = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 140)];
	[type setTextColor:[UIColor blackColor]];
	[type setBackgroundColor:[UIColor clearColor]];
	[type setFont:[UIFont fontWithName: @"Trebuchet MS" size: 14.0f]];
	type.hidden = YES;

	content = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 180)];
	[content setTextColor:[UIColor blackColor]];
	[content setBackgroundColor:[UIColor clearColor]];
	[content setFont:[UIFont fontWithName: @"Trebuchet MS" size: 14.0f]];
	content.hidden = YES;


	session = [[AVCaptureSession alloc] init];
	device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	input = [[AVCaptureDeviceInput alloc] init];
	output = [[AVCaptureMetadataOutput alloc] init];
	prevLayer = [[AVCaptureVideoPreviewLayer alloc] init];

	session = [[AVCaptureSession alloc] init];
	NSError *error = nil;

	input = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
	if (input) {
		[session addInput:input];
	} else {
		NSLog(@"Error: %@", error);
	}

	output = [[AVCaptureMetadataOutput alloc] init];
	[session addOutput:output];

	output.metadataObjectTypes = [output availableMetadataObjectTypes];

	prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
	prevLayer.frame = self.view.bounds;
	prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
	[self.view addSubview:content];
	[self.view addSubview:type];
	[self.view addSubview:webViewForScannerSearch];
	loadingView = [GraphicsLib returnLoadingViewInCenter:80 withHeight: 80];
	[self.view addSubview:loadingView];
	[self.view.layer addSublayer:prevLayer];
	[self.view addSubview:myScannerBox];
	[self.view addSubview:backgroundForSearchState];
	[self.view addSubview:backgroundForSearchStateBottom];
	[self.view addSubview: mySearchBarNav];
	[self.view addSubview:submitSearchButton];
	[self.view addSubview:backButtonForActivity];

	[self.view addSubview:clickScreenToDisplayEnlargedViewButtonOnClickNothing];
	clickScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = FALSE;

	self.view.backgroundColor = [UIColor whiteColor];
	[session startRunning];
	/*
	 Actions
	 */
	[scannerButton addTarget:self action:@selector(onClickToScannerCode:) forControlEvents:UIControlEventTouchDown];

	[scannerButton addTarget:self action:@selector(onClickScanAgainRelease:) forControlEvents:UIControlEventTouchUpInside];




	[submitSearchButton addTarget:self action:@selector(searchBarSearchButtonClickedSuppressed:) forControlEvents:UIControlEventTouchDown];
	[backButtonForActivity addTarget:self action:@selector(backButtonPressed:) forControlEvents:UIControlEventTouchDown];


	if(!barCodeScannerButtonStatus){
		prevLayer.hidden = true;
		myScannerBox.hidden = true;
		[session stopRunning];
		defaultLoadPageUrl = [NSString stringWithFormat:@"%@",urlSearch];
	}else{
		[self.view sendSubviewToBack:clickScreenToDisplayEnlargedViewButtonOnClickNothing];
	}
	[super.view bringSubviewToFront:myScannerBox];

	[webViewForScannerSearch setBackgroundColor:[UIColor whiteColor]];
	[super viewDidLoad];
	/*Delegates after load
	 */
	mySearchBarNav.delegate = self;
	[webViewForScannerSearch setDelegate:self];
	webViewForScannerSearch.scrollView.delegate = self;
	[output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
	barCodeScannerButtonStatus = NO;
}

+(void) clickElement:(id) element{
	[webViewForScannerSearch stringByEvaluatingJavaScriptFromString:@"goCompare()"];
}


+(void) setDefaultUrlLoad: (NSString *) append{
	defaultLoadPageUrl =[NSString stringWithFormat:@"%@%@", urlSearch, append];
	NSLog(@"%@", defaultLoadPageUrl);

}
- (void)backButtonPressed:(id) sender{

	if([webViewForScannerSearch canGoBack]){
		[webViewForScannerSearch goBack];
	}else{
		prevLayer.hidden = true;
		myScannerBox.hidden = true;
		[session stopRunning];

		UIViewController *presentingVC = [self presentingViewController];
		[self dismissViewControllerAnimated:NO completion:
		 ^{
			 [presentingVC presentViewController:vcWebViewController animated:YES completion:nil];
		 }];
	}
}

- (void)searchBarSearchButtonClickedSuppressed:(id) sender{
	if(![mySearchBarNav.text isEqualToString:@""] && ![mySearchBarNav.text isEqual:nil]){
		NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", urlSearch, [mySearchBarNav.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"], appendUrlClientSearch]]];
		NSLog(@"%@MyRequest",request);

		[webViewForScannerSearch loadRequest:request];
		NSLog(@"Search Bar Button Clicked");

		prevLayer.hidden = true;
		myScannerBox.hidden = true;
		[session stopRunning];
	}
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{

	NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", urlSearch, [mySearchBarNav.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"], appendUrlClientSearch]]];
	NSLog(@"%@MyRequest 2",request);

	[webViewForScannerSearch loadRequest:request];
	NSLog(@"Search Bar Button Clicked 2");

	prevLayer.hidden = true;
	myScannerBox.hidden = true;
	[session stopRunning];

}

-(void)onClickToScannerCode:(id)sender{
	UIButton *button = (UIButton *)sender;
	[GraphicsLib setButtonOpacityWithColor:button withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorOverAsString]]];
}

-(void)onClickScanAgainRelease:(id)sender{
	UIButton *button = (UIButton *)sender;
	[GraphicsLib setButtonOpacityWithColor:button withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorAsString]]];
	webViewForScannerSearch.hidden = true;
	prevLayer.hidden = false;
	[session startRunning];
	myScannerBox.hidden = false;
}

//disable zooming for delegating webviews
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return nil;
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	[webViewForScannerSearch reload];
}

-(void)onClickSearch:(id)sender{

}


- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
	NSString *detectionString = nil;
	NSString *codeType = nil;
	NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
							  AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
							  AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];

	for (AVMetadataObject *metadata in metadataObjects) {
		for (NSString *type in barCodeTypes) {
			if ([metadata.type isEqualToString:type])
			{
				detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
				if([metadata.type isEqualToString:AVMetadataObjectTypeEAN13Code]){
					if ([detectionString hasPrefix:@"0"] && [detectionString length] > 1)
						detectionString = [detectionString substringFromIndex:1];
					codeType = @"UPC_A";
				}
				NSLog(@"Type%@", codeType);
				break;
			}
		}

		if (detectionString != nil) {
			myScannerBox.hidden = true;
			NSLog(@"Detection string%@", detectionString);
			prevLayer.hidden = true;
			[session stopRunning];
			[content setText:detectionString];
			[type setText:codeType];

			NSString *newURL = [NSString stringWithFormat:@"%@%@%@", urlSearch, detectionString, appendUrlClientSearch];

			NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURL]];

			NSLog(@"Request from scanner%@", request);
			[webViewForScannerSearch loadRequest:request];
			webViewForScannerSearch.hidden = false;
		}
		else
			NSLog(@"BAD string%@", nil);
	}

}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	NSString *myURLGlobal = [request.URL absoluteString];

	if([myURLGlobal containsString:@"compareCescoNumbers"] && ![myURLGlobal containsString:@"www.rating-system.com"]){
		NSString * newURLGlobal = [NSString stringWithFormat:@"%@%@", myURLGlobal, @"&fromClient=true"];
		if([myURLGlobal containsString:@"&fromClient=true"]){
			return YES;
		}else{
			[webView loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURLGlobal]]];
			return NO;
		}
	}

	if([myURLGlobal containsString:@"defaultSearch"] && ![myURLGlobal containsString:@"www.rating-system.com"]){
		NSString * newURLGlobal = [NSString stringWithFormat:@"%@%@", myURLGlobal, @"/?fromClient=true"];
		if([myURLGlobal containsString:@"/?fromClient=true"]){
			return YES;
		}else{
			[webView loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURLGlobal]]];
			return NO;
		}
	}

	NSLog(@"%@ Redirect Url <-", myURLGlobal);
	if (navigationType == UIWebViewNavigationTypeLinkClicked) {
		NSURL *url = request.URL;
		NSString *scheme = [url scheme];
		//on click only
		if (![scheme containsString:appendUrlClientSearch] && IS_APPENDING) {
			NSString *newURL = [NSString stringWithFormat:@"%@%@", url, appendUrlClientSearch];
			NSLog(@"Scheme url%@", url);
			NSLog(@"app client url%@", newURL);
			[webView loadRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:newURL]]];
			return NO; // don't let the webview process it.
		}
	}
	return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
	loadingView.hidden = false;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
	[(UIScrollView *)[webView.subviews objectAtIndex:0] setScrollsToTop:YES];
	loadingView.hidden = true;

NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.innerHTML"];

	NSLog(@"%@ Search URL", webView.request.URL.absoluteString);
	if([html containsString:@"Compare"] && ![webView.request.URL.absoluteString containsString:@"compareReturnUrl"] && ![webView.request.URL.absoluteString containsString:@"productCompare?compareCescoNumbers"]){
		[self.view bringSubviewToFront:clickScreenToDisplayEnlargedViewButtonOnClickNothing];
		webView.scrollView.contentInset = UIEdgeInsetsMake(0.0,0.0,[GraphicsLib getHeightFromPercentageAsPixels:10],0.0);
		if(![WebViewController cookieValueIsNull]){
			clickScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = false;
		}else{
			clickScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = true;
		}
	}else{
		clickScreenToDisplayEnlargedViewButtonOnClickNothing.hidden = true;
		[self.view sendSubviewToBack:clickScreenToDisplayEnlargedViewButtonOnClickNothing];
	}
}

- (NSString*) urlRequestToString:(NSURLRequest*)urlRequest{
	NSString *requestPath = [[urlRequest URL] absoluteString];
	return requestPath;
}


-(NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskPortrait;
}


@end
