//
//  NSObject+GraphicsLib.h
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/10/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Resource.h"

@interface GraphicsLib : NSObject
{
    
}

+ (CGRect) returnButtonObject:(int)xPosition withYPosition:(int)yPosition withWidth:(int)width withHeight: (int)height;

+ (void) setButtonRounded: (UIButton*) myButton withCorner:(float)myRadius;

+ (void) setButtonOpacityWithColor: (UIButton*) myButton withOpacity:(float)myOpacity withBackgroundColor:(UIColor*) myBackgroundColor;

+ (void) setButtonBorderWithBorderColor: (UIButton*) myButton withBorderWidth:(int)myBorderWidth withBorderColor:(UIColor*) myBorderColor;

+(UIColor *)getColor:(NSString*)col;

+(void) setScreenWidth: (int) width;

+(void) setScreenHeight: (int) height;

+(int) getScreenWidth;

+(int) getScreenHeight;

+(void) setButtonAtXPosition:(UIButton *) myButton withNewAtX: (int) xCoordinate;

+(void) setButtonAtYPosition:(UIButton *) myButton withNewAtY: (int) YCoordinate;

+(void) setButtonAtXCenter:(UIButton *) myButton;

+(UIView *) getImageAsBackground: (NSString *) location;

+(UIView *) getImageasAsset: (NSString *) location withWidth:(int) width withHeight :(int) height withX: (int)xPosition withY:(int) yPosition;

+(int) getWidthFromPercentageAsPixels: (double) percent;

+(int) getHeightFromPercentageAsPixels: (double) percent;

+(void) setButtonToBottom: (UIButton*) myButton;

+(void) setButtonAtYCenter: (UIButton*) myButton;

+(UIView*) returnLoadingViewInCenter: (int) width withHeight:(int) height;

+(void) setButtonStyleForBottomMostButton: (UIButton *) myButton withText: (NSString*) myText;

+(void) setButtonStyleTwoForBottomMostButton: (UIButton *) myButton withText: (NSString*) myText;

+ (UIColor *) colorWithHexString: (NSString *) hexString;


@end
