//
//  TakePictureController.h
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 7/2/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphicsLib.h"
#import <MessageUI/MessageUI.h>
#import "ViewController.h"

@interface TakePictureController :UIViewController<UIImagePickerControllerDelegate>{

}

- (void)takePhoto:(id)sender;
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
-(void)mailComposeController:(MFMailComposeViewController *)mailer didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error;

+(UIImage * ) getImage ;


@end