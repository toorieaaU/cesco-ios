//
//  NSObject+GraphicsLib.m
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/10/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import "GraphicsLib.h"

@implementation GraphicsLib{
    
}

static int screenWidth;
static int screenHeight;

+(void) setScreenWidth: (int) width{
    screenWidth = width;
}

+(void) setScreenHeight: (int) height{
    screenHeight = height;
}

+(int) getScreenWidth{
    
    return screenWidth;
}

+(int) getScreenHeight{
    return screenHeight;
}

+ (CGRect) returnButtonObject:(int)xPosition withYPosition:(int)yPosition withWidth:(int)width withHeight: (int)height{
    return CGRectMake(xPosition, yPosition, width, height);
}

+ (void) setButtonRounded: (UIButton*) myButton withCorner:(float)myRadius{
    myButton.layer.cornerRadius = myRadius;
}

+ (void) setButtonOpacityWithColor: (UIButton*) myButton withOpacity:(float)myOpacity withBackgroundColor:(UIColor*) myBackgroundColor{
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha =0.0;
    [myBackgroundColor getRed:&red green:&green blue:&blue alpha:&alpha];
    
    [myButton setBackgroundColor:[UIColor colorWithRed:red green:green blue:blue alpha:myOpacity]];
}

+ (void) setButtonBorderWithBorderColor: (UIButton*) myButton withBorderWidth:(int)myBorderWidth withBorderColor:(UIColor*) myBorderColor{
    
    myButton.layer.borderWidth = myBorderWidth;
    myButton.layer.borderColor = myBorderColor.CGColor;
    
}

+(UIColor *)getColor:(NSString*)col{
    NSString *colLower = [col lowercaseString];
    NSString *retColor = [NSString stringWithFormat:@"%@%@", colLower, @"Color"];
    NSLog(@"%@",retColor);
    SEL selColor = NSSelectorFromString(retColor);
    UIColor *color = nil;
    if ( [UIColor respondsToSelector:selColor] == YES) {
        color = [UIColor performSelector:selColor];
    }
    
    
    return color;
}

+(void) setButtonAtXPosition:(UIButton *) myButton withNewAtX: (int) xCoordinate{
    CGRect newFrame = myButton.frame;
    newFrame.origin.x = xCoordinate;
    myButton.frame = newFrame;
}

+(void) setButtonAtYPosition:(UIButton *) myButton withNewAtY: (int) YCoordinate{
    CGRect newFrame = myButton.frame;
    newFrame.origin.y = YCoordinate;
    myButton.frame = newFrame;
}

+(void) setButtonAtXCenter:(UIButton *) myButton{
    CGRect newFrame = myButton.frame;
    newFrame.origin.x = ([GraphicsLib getScreenWidth]/2) - ((myButton.frame.size.width)/2);
    myButton.frame = newFrame;
}

+(UIView *) getImageAsBackground: (NSString *) location{
    UIImage *backgroundImage = [UIImage imageNamed:location];
    CGSize newSize = CGSizeMake([GraphicsLib getScreenWidth], [GraphicsLib getScreenHeight]);
    UIView *catView = [[UIView alloc] initWithFrame:CGRectMake(0,0,[GraphicsLib getScreenWidth],[GraphicsLib getScreenHeight])];
    UIGraphicsBeginImageContext( newSize );
    [backgroundImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:backgroundImage];
    imageView.frame = catView.bounds;
    
    [catView addSubview:imageView];
    
    return catView;
}

+(UIView *) getImageasAsset: (NSString *) location withWidth:(int) width withHeight :(int) height withX: (int)xPosition withY:(int) yPosition{
    UIImage *backgroundImage = [UIImage imageNamed:location];
    CGSize newSize = CGSizeMake(width, height);
    UIView *catView = [[UIView alloc] initWithFrame:CGRectMake(xPosition,yPosition,width,height)];
    UIGraphicsBeginImageContext( newSize );
    [backgroundImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:backgroundImage];
    imageView.frame = catView.bounds;
    
    [catView addSubview:imageView];
    
    return catView;
}

+(int) getWidthFromPercentageAsPixels: (double) percent{
    NSLog(@"Width%f", [GraphicsLib getScreenWidth] * (percent/100));
    return ([GraphicsLib getScreenWidth] * (percent/100));
}

+(int) getHeightFromPercentageAsPixels: (double) percent{
    NSLog(@"Width%f", [GraphicsLib getScreenHeight] * (percent/100));
    return ([GraphicsLib getScreenHeight] * (percent/100));
}

+(void) setButtonToBottom: (UIButton*) myButton{
    CGRect newFrame = myButton.frame;
    newFrame.origin.y = ([GraphicsLib getScreenHeight]) - ((myButton.frame.size.height));
    myButton.frame = newFrame;
}

//only partially tested.
+(void) setButtonAtYCenter: (UIButton*) myButton{
    CGRect newFrame = myButton.frame;
    newFrame.origin.y = ([GraphicsLib getScreenHeight]/2) - ((myButton.frame.size.height/2));
    myButton.frame = newFrame;
}

+(UIView*) returnLoadingViewInCenter: (int) width withHeight:(int) height{
    UIView *loadingView;
    loadingView = [[UIView alloc]initWithFrame:CGRectMake([GraphicsLib getScreenWidth]/2 - width/2, [GraphicsLib getScreenHeight]/2 - height/2, width, height)];
    loadingView.backgroundColor = [UIColor colorWithWhite:0. alpha:0.6];
    loadingView.layer.cornerRadius = 5;
    
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
    [activityView startAnimating];
    [loadingView addSubview:activityView];
    
    UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
    lblLoading.text = @"Loading...";
    lblLoading.textColor = [UIColor whiteColor];
    lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:15];
    lblLoading.textAlignment = NSTextAlignmentCenter;
    [loadingView addSubview:lblLoading];
    return loadingView;
}

+(void) setButtonStyleForBottomMostButton: (UIButton *) myButton withText: (NSString*) myText{
    //setWebViewStyle
    myButton.frame = [GraphicsLib returnButtonObject:0 withYPosition:0 withWidth:[GraphicsLib getWidthFromPercentageAsPixels:50] withHeight:[GraphicsLib getHeightFromPercentageAsPixels:10]];
    //set button rounded
    [GraphicsLib setButtonRounded:myButton withCorner:10];
    //set opacity and color
    [GraphicsLib setButtonOpacityWithColor:myButton withOpacity:.35 withBackgroundColor:[GraphicsLib getColor:[Resource getButtonColorAsString]]];
    //set button border and color
    [GraphicsLib setButtonBorderWithBorderColor:myButton withBorderWidth:1 withBorderColor:[UIColor blackColor]];
    //set button title
    [myButton setTitle:myText forState:(UIControlStateNormal)];
    //place button at middle position
    [GraphicsLib setButtonAtXCenter:myButton];
    //set button text color
    [myButton setTitleColor:[UIColor blackColor] forState: UIControlStateNormal];
    [GraphicsLib setButtonToBottom:myButton];
    myButton.frame =  CGRectMake( myButton.frame.origin.x,myButton.frame.origin.y - [GraphicsLib getHeightFromPercentageAsPixels:2],  myButton.frame.size.width, myButton.frame.size.height);
}

+(void) setButtonStyleTwoForBottomMostButton: (UIButton *) myButton withText: (NSString*) myText{
	//setWebViewStyle
	myButton.frame = [GraphicsLib returnButtonObject:0 withYPosition:0 withWidth:[GraphicsLib getWidthFromPercentageAsPixels:50] withHeight:[GraphicsLib getHeightFromPercentageAsPixels:10]];
	//set button rounded
	[GraphicsLib setButtonRounded:myButton withCorner:10];
	//set opacity and color
	[GraphicsLib setButtonOpacityWithColor:myButton withOpacity:1 withBackgroundColor:[UIColor orangeColor]];
	//set button border and color
	[GraphicsLib setButtonBorderWithBorderColor:myButton withBorderWidth:1 withBorderColor:[UIColor blackColor]];
	//set button title
	[myButton setTitle:myText forState:(UIControlStateNormal)];
	//place button at middle position
	[GraphicsLib setButtonAtXCenter:myButton];
	//set button text color
	[myButton setTitleColor:[UIColor whiteColor] forState: UIControlStateNormal];
	[GraphicsLib setButtonToBottom:myButton];
	myButton.frame =  CGRectMake( myButton.frame.origin.x,myButton.frame.origin.y - [GraphicsLib getHeightFromPercentageAsPixels:2],  myButton.frame.size.width, myButton.frame.size.height);
}

+ (UIColor *) colorWithHexString: (NSString *) hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString: @"#" withString: @""] uppercaseString];
    CGFloat alpha, red, blue, green;
    switch ([colorString length]) {
        case 3: // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 1];
            green = [self colorComponentFrom: colorString start: 1 length: 1];
            blue  = [self colorComponentFrom: colorString start: 2 length: 1];
            break;
        case 4: // #ARGB
            alpha = [self colorComponentFrom: colorString start: 0 length: 1];
            red   = [self colorComponentFrom: colorString start: 1 length: 1];
            green = [self colorComponentFrom: colorString start: 2 length: 1];
            blue  = [self colorComponentFrom: colorString start: 3 length: 1];
            break;
        case 6: // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom: colorString start: 0 length: 2];
            green = [self colorComponentFrom: colorString start: 2 length: 2];
            blue  = [self colorComponentFrom: colorString start: 4 length: 2];
            break;
        case 8: // #AARRGGBB
            alpha = [self colorComponentFrom: colorString start: 0 length: 2];
            red   = [self colorComponentFrom: colorString start: 2 length: 2];
            green = [self colorComponentFrom: colorString start: 4 length: 2];
            blue  = [self colorComponentFrom: colorString start: 6 length: 2];
            break;
        default:
            [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    return [UIColor colorWithRed: red green: green blue: blue alpha: alpha];
}

+ (CGFloat) colorComponentFrom: (NSString *) string start: (NSUInteger) start length: (NSUInteger) length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    return hexComponent / 255.0;
}

@end
