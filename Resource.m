//
//  NSObject+ResourceSingleton.m
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/10/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import "Resource.h"

@implementation Resource{

}

NSString *const MY_JSON_STYLE = @"https://api.myjson.com/bins/2tajo";
NSString *const ROOT_JSON_LIST_NAME = @"style";
NSString static * BUTTON_COLOR_ATTRIBUTE_NAME_COLOR = @"buttonColor";
NSString static * BUTTON_COLOR_ATTRIBUTE_NAME_COLOR_OVER = @"buttonColorHover";
NSString static * ICON_ONE_TEXT = @"iconOneText";
NSString * defaultButtonColor = @"RED";
NSString * defaultButtonColorOver = @"BLUE";
NSString * defaultIconOneText = @"Sign in for discounts";

+ (void)initialize
{
	static BOOL initialized = NO;
	if(!initialized)
	{
		initialized = YES;

		[self parseJsonStringAndSetFromString:BUTTON_COLOR_ATTRIBUTE_NAME_COLOR];
		[self parseJsonStringAndSetFromString:BUTTON_COLOR_ATTRIBUTE_NAME_COLOR_OVER];
		[self parseJsonStringAndSetFromString:ICON_ONE_TEXT];
	}
}


+(NSDictionary *) getJSONStringFromUrl: (NSString *) myUrlAsString{
	@try {
		NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:myUrlAsString]];
		NSData *theData = [NSURLConnection sendSynchronousRequest:request
												returningResponse:nil
															error:nil];

		NSDictionary *newJSON = [NSJSONSerialization JSONObjectWithData:theData
																options:0
																  error:nil];
		return newJSON;
	}@catch(NSException *exception){
		NSLog(@"%@", exception.reason);
		//default colors
		BUTTON_COLOR_ATTRIBUTE_NAME_COLOR = [[NSString alloc] initWithString:defaultButtonColor];
		BUTTON_COLOR_ATTRIBUTE_NAME_COLOR_OVER = [[NSString alloc] initWithString:defaultButtonColorOver];
		ICON_ONE_TEXT = [[NSString alloc] initWithString:defaultIconOneText];;
	}
}

+(void) parseJsonStringAndSetFromString: (NSString*) attribute {
	NSDictionary * myJsonDictionary = [self getJSONStringFromUrl:MY_JSON_STYLE];
	NSDictionary *dict=[[myJsonDictionary valueForKeyPath:ROOT_JSON_LIST_NAME] objectForKey:attribute];
	NSLog(@"JSON Capture: %@", [NSString stringWithFormat:@"%@", dict]);

	if([attribute isEqualToString:BUTTON_COLOR_ATTRIBUTE_NAME_COLOR]){
		BUTTON_COLOR_ATTRIBUTE_NAME_COLOR = [NSString stringWithFormat:@"%@", dict];
	}
	if([attribute isEqualToString:BUTTON_COLOR_ATTRIBUTE_NAME_COLOR_OVER]){
		BUTTON_COLOR_ATTRIBUTE_NAME_COLOR_OVER = [NSString stringWithFormat:@"%@", dict];
	}
	if([attribute isEqualToString:ICON_ONE_TEXT]){
		ICON_ONE_TEXT = [NSString stringWithFormat:@"%@", dict];
	}
}

+(NSString *) getButtonColorAsString{
	return BUTTON_COLOR_ATTRIBUTE_NAME_COLOR;
}

+(NSString *) getButtonColorOverAsString{
	return BUTTON_COLOR_ATTRIBUTE_NAME_COLOR_OVER;
}

+(NSString *) getIconOneText{
	return ICON_ONE_TEXT;
}

@end
