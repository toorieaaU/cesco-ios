//
//  NSObject+ResourceSingleton.h
//  UnboundCommerceIosPlatform
//
//  Created by Anthony Ameer Toorie on 6/10/15.
//  Copyright (c) 2015 Anthony Ameer Toorie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Resource : NSObject{

}
+(NSDictionary *) getJSONStringFromUrl: (NSString *) myUrlAsString;
+(void) parseJsonStringAndSetFromString: (NSString*) attribute;

+(NSString *) getButtonColorAsString;
+(NSString *) getButtonColorOverAsString;
+(NSString *) getIconOneText;
@end

